from django.urls import reverse_lazy
from django.views.generic import CreateView
from django.http import JsonResponse

from .forms import CustomUserCreationForm
from shops.models import Shop

class SignUpView(CreateView):
	form_class = CustomUserCreationForm
	success_url = reverse_lazy('login')
	template_name = 'signup.html'

def likeView(request):
	current_user = request.user
	shop_id = request.GET.get('shop_id', None)
	shop = Shop.objects.get(id=shop_id)
	current_user.shops.add(shop)
	current_user.save()
	data = {
		'success': True
	}
	return JsonResponse(data)

def deleteView(request):
	current_user = request.user
	shop_id = request.GET.get('shop_id', None)
	shop = Shop.objects.get(id=shop_id)
	current_user.shops.remove(shop)
	current_user.save()
	data = {
		'success': True
	}
	return JsonResponse(data)

def dislikeView(request):
	current_user = request.user
	shop_id = request.GET.get('shop_id', None)
	shop = Shop.objects.get(id=shop_id)
	current_user.shops.remove(shop)
	current_user.save()
	data = {
		'success': True
	}
	return JsonResponse(data)