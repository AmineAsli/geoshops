from django.contrib.auth.models import AbstractUser
from django.db import models
from shops.models import Shop

class CustomUser(AbstractUser):
	shops = models.ManyToManyField(Shop)