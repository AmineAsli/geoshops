from .models import CustomUser
from django.test import Client, TestCase
from django.urls import reverse

from shops.models import Shop

class AccountsTests(TestCase):

	def setUp(self):
		self.user = CustomUser.objects.create_user(
			username='testuser',
			email='test@email.com',
			password='secret'
		)

	def test_login_page(self):
		response = self.client.get(reverse('login'))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'registration/login.html')

	def test_signup_page(self):
		response = self.client.get(reverse('signup'))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'signup.html')

	def test_logout_redirection(self):
		response = self.client.get(reverse('logout'))
		self.assertEqual(response.status_code, 302)
		self.assertRedirects(response, reverse('home'))

	def test_user_login(self):
		user = self.client.login(username='testuser', password='secret')
		# Check our user is logged in
		self.assertTrue(user)

	def test_add_to_preferred_shops(self):
		shop1 = Shop.objects.get(id=1)
		shop2 = Shop.objects.get(id=2)

		self.user.shops.add(shop1)
		self.user.shops.add(shop2)
		
		self.assertEqual(self.user.shops.all().count(), 2)

	def test_remove_from_preferred_shops(self):
		shop1 = Shop.objects.get(id=1)
		shop2 = Shop.objects.get(id=2)

		self.user.shops.add(shop1)
		self.user.shops.add(shop2)
		
		self.assertEqual(self.user.shops.all().count(), 2)

		self.user.shops.remove(shop2)

		self.assertEqual(self.user.shops.all().count(), 1)


