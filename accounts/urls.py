from django.urls import path
from .views import SignUpView, likeView, dislikeView, deleteView

urlpatterns = [
	path('signup/', SignUpView.as_view(), name='signup'),
	path('ajax/like/', likeView, name='like'),
	#path('ajax/dislike/', dislikeView, name='dislike'),
	path('ajax/delete/', deleteView, name='delete'),
]