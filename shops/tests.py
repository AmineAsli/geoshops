from shops.models import Shop
from django.test import TestCase
from django.contrib.gis.geos import fromstr
from django.urls import reverse
from django.contrib.gis.db.models.functions import Distance


from accounts.models import CustomUser

class ShopsTests(TestCase):
	
	def setUp(self):
	
		self.client.force_login(CustomUser.objects.get_or_create(username='testuser')[0])

		self.longitude = "34.0177194" 
		self.latitude = "-6.8288437"
		self.location = fromstr(f'POINT({self.longitude} {self.latitude})', srid=4326)

	def test_shops_page(self):
		response = self.client.get(reverse('shops'))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'shops.html')

	def test_preffered_shops_page(self):
		response = self.client.get(reverse('preffered_shops'))
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'preferred_shops.html')

	def test_shop_model(self):
		
		shop1 = Shop.objects.get(name="Mega Mall")
		self.assertEqual(shop1.location, self.location)

		address = '111, avenue Fal Ould Oumeir, Agdal'

		shop2 = Shop.objects.get(name="DEVRED 1902")
		self.assertEqual(shop2.address, address)

	def test_distance_calculation(self):
		shops = []
		shops_result = ['Mega Mall', 'Rip Curl Surf Shop', 'PRODUCTION MXGP SHOP RABAT']
		for shop in Shop.objects.annotate(distance=Distance('location', self.location)).order_by('distance')[0:3]:
			shops.append(shop.name)
			
		self.assertEqual(shops_result, shops)