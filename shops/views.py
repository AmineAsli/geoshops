from django.views import generic
from django.contrib.gis.geos import Point
from django.contrib.gis.db.models.functions import Distance
import geocoder

from .models import Shop

class ShopsView(generic.ListView):

	model = Shop
	context_object_name = 'shops'
	template_name = 'shops.html'

	def get_queryset(self):
		g = geocoder.ip('me')
		user_location = Point(g.latlng[1], g.latlng[0], srid=4326)
		return Shop.objects.annotate(distance=Distance('location',
    user_location)).order_by('distance').exclude(customuser=self.request.user)
    


class PreferredShopsView(generic.ListView):
	template_name = 'preferred_shops.html'
	context_object_name = 'shops'

	def get_queryset(self):
		return Shop.objects.filter(customuser=self.request.user)