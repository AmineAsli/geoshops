from django.urls import path

from .views import ShopsView, PreferredShopsView

urlpatterns = [
	path('', ShopsView.as_view(), name='shops'),
	path('preffered/', PreferredShopsView.as_view(), name='preffered_shops'),
]